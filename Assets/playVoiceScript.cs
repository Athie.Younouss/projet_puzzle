using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Windows.Speech;

public class playVoiceScript : MonoBehaviour
{
    private KeywordRecognizer keywordRecognizer;
    private Dictionary<string, Action> actions = new Dictionary<string, Action>();

    // Stocke la pi�ce actuellement s�lectionn�e
    private TileMovement selectedTileMovement;

    public Button buttonPlay;

    void Start()
    {
        buttonPlay = GetComponent<Button>();

        if (buttonPlay != null )
        {
            Debug.Log("Button play getted");
        }

        actions.Add("play", playGame);

        // Initialisation de la reconnaissance vocale
        keywordRecognizer = new KeywordRecognizer(actions.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += RecognizedSpeech;
        keywordRecognizer.Start();
    }

    void Update()
    {

    }

    private void RecognizedSpeech(PhraseRecognizedEventArgs speech)
    {
        Debug.Log("Recognized command: " + speech.text);
        actions[speech.text].Invoke();
    }

    void playGame()
    {
        buttonPlay.onClick.Invoke();
    }
}