using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Windows.Speech;
using Tobii.Research.Unity;
using System.Collections;

public class VoiceCommandManager : MonoBehaviour
{
    private KeywordRecognizer keywordRecognizer;
    private Dictionary<string, Action> actions = new Dictionary<string, Action>();

    // Stocke la pi�ce actuellement s�lectionn�e
    private TileMovement selectedTileMovement;

    private Button playButton;

    //private GazeVisualizer gazeVisualizer;

    // Pour v�rifier la stabilit� de l'indicateur
    //private Coroutine gazeCheckCoroutine;

    void Start()
    {
        playButton = GameObject.Find("ButtonPlay").GetComponent<Button>();


        if (playButton != null )
        {
            Debug.Log("VoiceMovement got button play");
        }

        // Ajout des commandes vocales
        actions.Add("select", PutCommand);
        actions.Add("choose", PutCommand);
        actions.Add("drag", PutCommand);
        actions.Add("take", PutCommand);
        actions.Add("that", PutCommand);
        actions.Add("there", DropCommand);
        actions.Add("here", DropCommand);
        actions.Add("drop", DropCommand);

        actions.Add("play", startCommand);
        actions.Add("start", startCommand);
        actions.Add("go", startCommand);


        // Initialisation de la reconnaissance vocale
        keywordRecognizer = new KeywordRecognizer(actions.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += RecognizedSpeech;
        keywordRecognizer.Start();

        // R�f�rence � GazeVisualizer // Trouver le GazeVisualizer dans la sc�ne
        //gazeVisualizer = FindObjectOfType<GazeVisualizer>();

        //if (gazeVisualizer == null)
        //{
        //Debug.LogError("GazeVisualizer not found in the scene.");
        // }

    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("Key 'A' pressed, simulating 'put' command");
            PutCommand();
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            Debug.Log("Key 'B' pressed, simulating 'there' command");
            DropCommand();
        }
    }

    private void RecognizedSpeech(PhraseRecognizedEventArgs speech)
    {
        Debug.Log("Recognized command: " + speech.text);
        actions[speech.text].Invoke();
    }

    private void PutCommand()
    {
        Debug.Log("Command 'put' detected");

        if (GameApp.Instance.curent_tile != null)
        {
            selectedTileMovement = GameApp.Instance.curent_tile;
            Vector2 gazePosition = GameApp.Instance.gazeVisualizer.GetScreenGazePosition();
            selectedTileMovement.SelectPiece(gazePosition);
            Debug.Log("Piece selected: " + selectedTileMovement.name);
            GameApp.Instance.voice_started = true;
        }
        else
        {
            Debug.LogWarning("No tile currently under gaze.");
        }
    }


    private void DropCommand()
    {
        Debug.Log("Command 'there' detected");
        Debug.Log((selectedTileMovement != null).ToString());
        Debug.Log(GameApp.Instance.voice_started.ToString());

        if (selectedTileMovement != null && GameApp.Instance.voice_started)
        {
            Vector2 gazePos = GameApp.Instance.gazeVisualizer.GetScreenGazePosition();
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(new Vector3(gazePos.x, gazePos.y, Camera.main.nearClipPlane));
            selectedTileMovement.DropPiece(worldPos);
            Debug.Log("Piece dropped at: " + worldPos);
            selectedTileMovement = null;
            GameApp.Instance.voice_started = false;
        }
        else
        {
            Debug.LogWarning("No piece selected to drop.");
        }
    }

    private void startCommand()
    {
        {
            playButton.onClick.Invoke();
        }
    }
}