using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Tobii.Research.Unity;

//[RequireComponent(typeof(Collider2D))]
public class TileMovement : MonoBehaviour
{
    public Tile tile { get; set; }
    private Vector3 mOffset = new Vector3(0.0f, 0.0f, 0.0f);
    private SpriteRenderer mSpriteRenderer;

    public delegate void DelegateOnTilePlaced(TileMovement tm);
    public static event DelegateOnTilePlaced OnTilePlacedCorrectly;
    public static event DelegateOnTilePlaced OnTilePlacedIncorrectly;
    public static event System.Action OnTileSelectionDelayed;

    private Coroutine selectionDelayCoroutine;
    private bool isPlacedCorrectly = false;

    private Color originalColor;
    private Color highlightColor = Color.yellow; // Couleur de surbrillance

    //private GazeVisualizer gazeVisualizer;

    //public float stabilizationDelay = 2.0f;
    //private Coroutine gazeHoldCoroutine;

    void Start()
    {
        mSpriteRenderer = GetComponent<SpriteRenderer>();
        originalColor = mSpriteRenderer.color;
        //gazeVisualizer = FindObjectOfType<GazeVisualizer>();

        //if (gazeVisualizer == null)
        // {
        // Debug.LogError("GazeVisualizer not found");
        //}
    }

    private Vector3 GetCorrectPosition()
    {
        return new Vector3(tile.xIndex * 100f, tile.yIndex * 100f, 0f);
    }

    public void HighlightPiece(bool highlight)
    {
        mSpriteRenderer.color = highlight ? highlightColor : originalColor;
    }

    public void SelectPiece(Vector2 gazePosition)
    {
        if (!GameApp.Instance.TileMovementEnabled || isPlacedCorrectly) return;

        if (selectionDelayCoroutine != null)
        {
            StopCoroutine(selectionDelayCoroutine);
        }

        mOffset = transform.position - Camera.main.ScreenToWorldPoint(
            new Vector3(gazePosition.x, gazePosition.y, Camera.main.nearClipPlane));

        Tile.tilesSorting.BringToTop(mSpriteRenderer);
        Debug.Log("Piece selected: " + name);

        StartSelectionDelayCoroutine(); // D�marrer le timer lors de la s�lection d'une pi�ce
    }

    public void DropPiece(Vector3 position)
    {
        if (!GameApp.Instance.TileMovementEnabled || isPlacedCorrectly) return;

        transform.position = position + mOffset;

        float dist = (transform.position - GetCorrectPosition()).magnitude;
        if (dist < 20.0f)
        {
            transform.position = GetCorrectPosition();
            if (!isPlacedCorrectly)
            {
                isPlacedCorrectly = true;
                OnTilePlacedCorrectly?.Invoke(this);
                Debug.Log("Piece correctly placed: " + name);
            }
        }
        else
        {
            OnTilePlacedIncorrectly?.Invoke(this);
            Debug.Log("Piece incorrectly placed: " + name);
        }

        StartSelectionDelayCoroutine(); // Red�marrer le timer lors du d�p�t d'une pi�ce
    }


    void StartSelectionDelayCoroutine()
    {
        if (selectionDelayCoroutine != null)
        {
            StopCoroutine(selectionDelayCoroutine);
        }
        selectionDelayCoroutine = StartCoroutine(SelectionDelayCoroutine());
    }

    private IEnumerator SelectionDelayCoroutine()
    {
        yield return new WaitForSeconds(20);
        OnTileSelectionDelayed?.Invoke();
    }

    void Update()
    {
        // V�rifie si le gaze est sur cette tuile
        Vector2 gazePosition = GameApp.Instance.gazeVisualizer.GetScreenGazePosition();
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(new Vector3(gazePosition.x, gazePosition.y, Camera.main.nearClipPlane));
        Collider2D hitCollider = Physics2D.OverlapPoint(worldPoint);
        if (hitCollider != null && hitCollider.gameObject == gameObject)
        {
            if (!GameApp.Instance.voice_started && GameApp.Instance.curent_tile != this)
            {
                GameApp.Instance.curent_tile = this;
            }
        }
        else if (GameApp.Instance.curent_tile == this)
        {
            if (!GameApp.Instance.voice_started)
            {
                GameApp.Instance.curent_tile = null;
            }
        }
    }
}
