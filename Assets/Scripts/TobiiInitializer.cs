using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Research.Unity;

public class TobiiInitializer : MonoBehaviour
{
    void Start()
    {
        if (EyeTracker.Instance == null || !EyeTracker.Instance.SubscribeToGazeData)
        {
            Debug.LogError("Tobii Eye Tracker not connected.");
        }
        else
        {
            Debug.Log("Tobii Eye Tracker connected!");
        }
    }
}
