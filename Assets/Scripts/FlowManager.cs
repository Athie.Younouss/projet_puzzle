using System;
using UnityEngine;

public class HRVManager : MonoBehaviour
{
    public static event Action<bool> OnStressLevelChanged;
    private bool isPlayerStressed = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            SimulateStressLevelChange(true); // Simule un niveau de stress �lev�
        }
        if (Input.GetKeyDown(KeyCode.Y))
        {
            SimulateStressLevelChange(false); // Simule un niveau de stress normal
        }
    }

    public void SimulateStressLevelChange(bool isStressed)
    {
        isPlayerStressed = isStressed;
        OnStressLevelChanged?.Invoke(isStressed);
    }
}