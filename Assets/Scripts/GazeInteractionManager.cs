using UnityEngine;
using Tobii.Gaming;

public class GazeInteractionManager : MonoBehaviour
{
    public GameObject highlightObject;

    void Update()
    {
        GazePoint gazePoint = TobiiAPI.GetGazePoint();
        if (gazePoint.IsValid)
        {
            Vector3 screenPosition = new Vector3(gazePoint.Screen.x, gazePoint.Screen.y, Camera.main.nearClipPlane);
            Vector3 worldPosition = Camera.main.ScreenToWorldPoint(screenPosition);
            highlightObject.transform.position = new Vector3(worldPosition.x, worldPosition.y, highlightObject.transform.position.z);
        }
    }
}
