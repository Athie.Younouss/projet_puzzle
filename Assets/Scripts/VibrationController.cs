using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class VibrationController : MonoBehaviour
{
    private Gamepad gamepad;

    private void Start()
    {
        gamepad = Gamepad.current;
        if (gamepad == null)
        {
            Debug.LogError("No gamepad connected!");
        }
    }

    public void TriggerVibration(TactonConfig.TactonType tactonType)
    {
        if (gamepad == null)
        {
            Debug.LogError("No gamepad connected!");
            return;
        }

        TactonConfig.Tacton tacton = TactonConfig.GetTacton(tactonType);
        if (tacton != null)
        {
            StartCoroutine(ApplyVibrationPattern(tacton));
        }
        else
        {
            Debug.LogError("Tacton is null for type: " + tactonType);
        }
    }

    private IEnumerator ApplyVibrationPattern(TactonConfig.Tacton tacton)
    {
        for (int i = 0; i < tacton.Amplitudes.Count; i++)
        {
            //Debug.Log($"VibrationPattern - Pulse {i + 1}: Amplitude={tacton.Amplitudes[i]}, Duration={tacton.Durations[i]}, Frequency={tacton.Frequencies[i]}");

            gamepad.SetMotorSpeeds(tacton.Amplitudes[i], tacton.Amplitudes[i]);
            yield return new WaitForSeconds(tacton.Durations[i] / 1000f); // Convert ms to seconds
            gamepad.SetMotorSpeeds(0, 0);
            yield return new WaitForSeconds(tacton.Interval / 1000f); // Convert ms to seconds
        }
        StopVibrations();
    }

    public void StopVibrations()
    {
        if (gamepad != null)
        {
            gamepad.SetMotorSpeeds(0, 0);
        }
    }
}