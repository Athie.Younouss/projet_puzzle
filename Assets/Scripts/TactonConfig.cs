using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Static class to manage and retrieve Tacton configurations.
/// </summary>
public static class TactonConfig
{
    /// <summary>
    /// Enumeration of different Tacton types.
    /// </summary>
    public enum TactonType
    {
        CorrectPlacement,
        PlacementError,
        TimeDelay
    }

    /// <summary>
    /// Class representing a Tacton configuration.
    /// </summary>
    public class Tacton
    {
        public List<float> Amplitudes { get; private set; }
        public List<int> Durations { get; private set; }
        public List<int> Frequencies { get; private set; }
        public int Interval { get; private set; }

        /// <summary>
        /// Constructor for Tacton.
        /// </summary>
        /// <param name="amplitudes">List of amplitudes for the Tacton pattern.</param>
        /// <param name="durations">List of durations for the Tacton pattern.</param>
        /// <param name="frequencies">List of frequencies for the Tacton pattern.</param>
        /// <param name="interval">Interval between pulses in milliseconds.</param>
        public Tacton(List<float> amplitudes, List<int> durations, List<int> frequencies, int interval)
        {
            if (amplitudes == null || durations == null || frequencies == null)
            {
                throw new System.ArgumentException("Tacton parameters cannot be null");
            }
            if (amplitudes.Count != durations.Count || amplitudes.Count != frequencies.Count)
            {
                throw new System.ArgumentException("All Tacton parameter lists must have the same length");
            }

            Amplitudes = amplitudes;
            Durations = durations;
            Frequencies = frequencies;
            Interval = interval;
        }
    }

    private static Dictionary<TactonType, Tacton> tactonDictionary;

    static TactonConfig()
    {
        InitializeTactons();
    }

    private static void InitializeTactons()
    {
        tactonDictionary = new Dictionary<TactonType, Tacton>
        {
            {
                TactonType.CorrectPlacement,
                new Tacton(
                    new List<float> { 1.0f },
                    new List<int> { 500 },
                    new List<int> { 250 },
                    0
                )
            },
            {
                TactonType.PlacementError,
                new Tacton(
                    new List<float> { 1.0f, 0.8f, 1.0f },
                    new List<int> { 300, 300, 300 },
                    new List<int> { 150, 200, 150 },
                    100
                )
            },
            {
                TactonType.TimeDelay,
                new Tacton(
                    new List<float> { 0.8f, 1.0f, 0.8f, 1.0f },
                    new List<int> { 400, 600, 400, 600 },
                    new List<int> { 100, 200, 300, 400 },
                    200
                )
            }
        };
    }

    /// <summary>
    /// Retrieves a Tacton configuration based on the given type.
    /// </summary>
    /// <param name="type">Type of the Tacton.</param>
    /// <returns>Tacton configuration.</returns>
    public static Tacton GetTacton(TactonType type)
    {
        if (tactonDictionary.TryGetValue(type, out Tacton tacton))
        {
            return tacton;
        }
        else
        {
            Debug.LogError("Tacton not found: " + type.ToString());
            return null;
        }
    }
}