using UnityEngine;

public class VibrationEventManager : MonoBehaviour
{
    private VibrationController vibrationController;

    private void OnEnable()
    {
        TileMovement.OnTilePlacedCorrectly += HandleCorrectPlacement;
        TileMovement.OnTilePlacedIncorrectly += HandlePlacementError;
        TileMovement.OnTileSelectionDelayed += HandleSelectionDelay;
    }

    private void OnDisable()
    {
        TileMovement.OnTilePlacedCorrectly -= HandleCorrectPlacement;
        TileMovement.OnTilePlacedIncorrectly -= HandlePlacementError;
        TileMovement.OnTileSelectionDelayed -= HandleSelectionDelay;
    }

    private void Start()
    {
        vibrationController = FindObjectOfType<VibrationController>();
        if (vibrationController == null)
        {
            Debug.LogError("VibrationController not found in the scene!");
        }
    }

    public void HandleCorrectPlacement(TileMovement tm)
    {
        if (vibrationController != null)
        {
            vibrationController.StopVibrations();
            vibrationController.TriggerVibration(TactonConfig.TactonType.CorrectPlacement);
        }
    }

    public void HandlePlacementError(TileMovement tm)
    {
        if (vibrationController != null)
        {
            vibrationController.StopVibrations();
            vibrationController.TriggerVibration(TactonConfig.TactonType.PlacementError);
        }
    }

    public void HandleSelectionDelay()
    {
        if (vibrationController != null)
        {
            vibrationController.StopVibrations();
            vibrationController.TriggerVibration(TactonConfig.TactonType.TimeDelay);
        }
    }
}