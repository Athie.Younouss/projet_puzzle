using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bitalino : MonoBehaviour
{
    private PluxDeviceManager deviceManager;

    public void ScanResults(List<string> listDevices)
    {
        Debug.Log("Devices found: " + listDevices.Count);
        if (listDevices.Count == 0)
        {
            Debug.Log("No devices found, trying again...");
            deviceManager.GetDetectableDevicesUnity(domains);
            return;
        }

        foreach (string device in listDevices)
        {
            if(device.Contains(BITALINO_MAC))
            {
                Debug.Log("Trying to connect to " + device);
                deviceManager.PluxDev(device);
               // Debug.Log("Product ID Unity" + deviceManager.GetProductIdUnity().ToString());
            }

            Debug.Log("Device: " + device);
        }   
    }

    public void ConnectionDone(bool connectionStatus)
    {
        if (connectionStatus) {
            Debug.Log("Product ID Unity" + deviceManager.GetProductIdUnity().ToString());
            deviceManager.StartAcquisitionUnity(BITALINO_FREQUENCY, new List<int>() { 2 }, 10);
        }
        Debug.Log("Connection status: " + connectionStatus);
    }

    public void AcquisitionStarted(bool acquisitionStatus, bool exceptionRaised, string exceptionMessage)
    {
        Debug.Log("Acquisition status: " + acquisitionStatus);
        if (exceptionRaised)
        {
            Debug.Log("Exception raised: " + exceptionMessage);
        }
    }

    public void OnDataReceived(int nSeq, int[] data)
    {
        // write csv file
        string content = nSeq + "," + data[0];
        System.IO.File.AppendAllText(path, content + System.Environment.NewLine);

        /*Debug.Log("Data received: " + nSeq);
        foreach (int value in data)
        {
            Debug.Log("Value: " + value);
        }*/
    }

    public void OnEventDetected(PluxDeviceManager.PluxEvent pluxEvent)
    {
        Debug.Log("Event detected: " + pluxEvent);
    }

    public void OnExceptionRaised(int exceptionCode, string exceptionDescription)
    {
        Debug.Log("Exception raised: " + exceptionCode + " - " + exceptionDescription);
    }

    // Class constants (CAN BE EDITED BY IN ACCORDANCE TO THE DESIRED DEVICE CONFIGURATIONS)
    [System.NonSerialized]
    public List<string> domains = new List<string>() { "BTH" };

    private const int BITALINO_FREQUENCY = 1000;
    private const string BITALINO_MAC = "00:21:06:BE:15:D9";
    private const int BITALINO_PID = 1538;

    private int fileId = 0;

    private string path = "";

    void Start()
    {
        Debug.Log("Bitalino Start()");

        fileId = DateTime.Now.Hour * 3600 + DateTime.Now.Minute * 60 + DateTime.Now.Second;
        // create csv file with specifi id fileId
        path = $"Assets/data_{fileId}.csv";
        System.IO.File.WriteAllText(path, "nSeq,ECG" + System.Environment.NewLine);


        deviceManager = new PluxDeviceManager(
            ScanResults,
            ConnectionDone,
            AcquisitionStarted,
            OnDataReceived,
            OnEventDetected,
            OnExceptionRaised
        );

        deviceManager.GetDetectableDevicesUnity(domains);

        /*deviceManager.PluxDev(BITALINO_MAC);
        deviceManager.StartAcquisitionUnity(BITALINO_FREQUENCY, new List<int>(2), 1);*/
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
