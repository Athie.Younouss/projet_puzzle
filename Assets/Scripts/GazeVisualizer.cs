using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tobii.Research.Unity;

public class GazeVisualizer : MonoBehaviour
{
    public Image gazeIndicator; // Drag and drop the GazeIndicator UI Image here in the Inspector
    private Vector3 lastValidGazePoint = Vector3.zero;
    private float smoothingFactor = 0.1f; // Augmenter le facteur de lissage

    void Update()
    {
        if (EyeTracker.Instance != null)
        {
            IGazeData lastGazeData = EyeTracker.Instance.LatestGazeData;
            if (lastGazeData.CombinedGazeRayScreenValid)
            {
                Vector3 gazePos = lastGazeData.CombinedGazeRayScreen.GetPoint(10); // Distance from camera
                Vector3 smoothedGazePos = Vector3.Lerp(lastValidGazePoint, gazePos, smoothingFactor);
                lastValidGazePoint = smoothedGazePos;

                Vector2 screenPoint = Camera.main.WorldToScreenPoint(smoothedGazePos);

                // Update the position of the gaze indicator
                gazeIndicator.transform.position = screenPoint;

                // Debug.Log("Gaze Position: " + screenPoint);
            }
            else
            {
                //Debug.LogWarning("Gaze data not valid.");
            }
        }
    }

    public Vector3 GetScreenGazePosition()
    {
        return Camera.main.WorldToScreenPoint(lastValidGazePoint);
    }
}
