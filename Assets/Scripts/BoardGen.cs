using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public class BoardGen : MonoBehaviour
{
    private string imageFilename;
    Sprite mBaseSpriteOpaque;
    Sprite mBaseSpriteTransparent;

    GameObject mGameObjectOpaque;
    GameObject mGameObjectTransparent;
    GameObject mGameObjectSectioned;

    public float ghostTransparency = 0.1f;

    public int numTileX { get; private set; }
    public int numTileY { get; private set; }

    Tile[,] mTiles = null;
    GameObject[,] mTileGameObjects = null;
    GameObject[,] mTileGOExample = null;
    Vector3[,] endPositions = null;

    public Transform parentForTiles = null;

    public Menu menu = null;
    private List<Rect> regions = new List<Rect>();
    private List<Coroutine> activeCoroutines = new List<Coroutine>();

    private VibrationEventManager vibrationEventManager;
    private Coroutine timerCoroutine;
    private Coroutine timerCoroutineHide;

    private bool isPlayerStressed = false;

    private float timerSecondHelp = 10f;

    void Start()
    {
        imageFilename = GameApp.Instance.GetJigsawImageName();

        mBaseSpriteOpaque = LoadBaseTexture();
        mGameObjectOpaque = new GameObject();
        mGameObjectOpaque.name = imageFilename + "_Opaque";
        var spriteRenderer = mGameObjectOpaque.AddComponent<SpriteRenderer>();
        spriteRenderer.sprite = mBaseSpriteOpaque;
        spriteRenderer.sortingLayerName = "Opaque";
        spriteRenderer.color = new Color(1f, 1f, 1f, ghostTransparency);  // Initialiser avec une transparence r�duite

        mBaseSpriteTransparent = CreateTransparentView(mBaseSpriteOpaque.texture);
        mGameObjectTransparent = new GameObject();
        mGameObjectTransparent.name = imageFilename + "_Transparent";
        mGameObjectTransparent.AddComponent<SpriteRenderer>().sprite = mBaseSpriteTransparent;
        mGameObjectTransparent.GetComponent<SpriteRenderer>().sortingLayerName = "Transparent";

        // Cr�er un nouvel objet pour l'image sectionn�e visible avant le clic sur "Play"
        mGameObjectSectioned = new GameObject();
        mGameObjectSectioned.name = imageFilename + "_Sectioned";
        mGameObjectSectioned.AddComponent<SpriteRenderer>().sprite = mBaseSpriteOpaque; // Utiliser la m�me image que pour l'image opaque
        mGameObjectSectioned.GetComponent<SpriteRenderer>().sortingLayerName = "Sectioned";
        mGameObjectSectioned.SetActive(false); // Le garder d�sactiv� au d�but

        mGameObjectOpaque.SetActive(true);  // Assurez-vous qu'il est actif au d�but

        SetCameraPosition();

        StartCoroutine(Coroutine_CreateJigsawTiles());

        vibrationEventManager = FindObjectOfType<VibrationEventManager>();

        HRVManager.OnStressLevelChanged += HandleStressLevelChange;
    }

    private IEnumerator TimerCoroutine(float delay, System.Action callback)
    {
        // Wait for the specified delay (10 seconds in this case)
        yield return new WaitForSeconds(delay);

        // Execute the callback function
        callback?.Invoke();
    }

    private void OnHelpTimerComplete()
    {
        // This function will be called after the 10 seconds have passed
        ShowSectionedImage();
        HighlightPieceAndLocation();
        StartStopFlowTimer();
    }

    private void OnHelpTimerHideComplete()
    {
        // This function will be called after the 10 seconds have passed
        HideSectionedImage();
        SetTransparencyOfBackground(ghostTransparency);
        ClearHighlight();

        RestartTimer();
    }


    public void StartFlowTimer()
    {
        // Start the timer coroutine with a 10-second delay
        timerCoroutine = StartCoroutine(TimerCoroutine(timerSecondHelp, OnHelpTimerComplete));
    }
    public void StartStopFlowTimer() 
    {
        timerCoroutineHide = StartCoroutine(TimerCoroutine(5f, OnHelpTimerHideComplete));
    }

    public void RestartTimer()
    {
        // Stop the existing timer if it's running
        if (timerCoroutine != null)
        {
            StopCoroutine(timerCoroutine);
        }

        if (timerCoroutineHide != null)
        {
            StopCoroutine(timerCoroutineHide);
        }

        // Start a new 10-second timer
        StartFlowTimer();
    }

    void OnDestroy()
    {
        HRVManager.OnStressLevelChanged -= HandleStressLevelChange;
    }

    void HandleStressLevelChange(bool isStressed)
    {
        if (isPlayerStressed != isStressed)
        {
            isPlayerStressed = isStressed;
            AdjustGameDifficulty(isStressed);
        }
    }

    void AdjustGameDifficulty(bool isStressed)
    {
        if (isPlayerStressed)
        {
            ShowSectionedImage();
            HighlightPieceAndLocation();
        }
        else
        {
            HideSectionedImage();
            SetTransparencyOfBackground(ghostTransparency);
            ClearHighlight();
        }
    }

    void SetTransparencyOfBackground(float alpha)
    {
        var spriteRenderer = mGameObjectTransparent.GetComponent<SpriteRenderer>();
        spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, alpha);
    }

    void ShowSectionedImage()
    {
        //foreach (var example_piece in mTileGOExample) { 
        //    example_piece.SetActive(true);
        //}
        //mGameObjectSectioned.SetActive(true);  // Afficher l'image sectionn�e

        // Masquer l'image transparente pour que seule l'image sectionn�e apparaisse
        mGameObjectTransparent.SetActive(false);
    }

    void HideSectionedImage()
    {
        foreach (var example_piece in mTileGOExample)
        {
            example_piece.SetActive(false);
        }
        //mGameObjectSectioned.SetActive(false);  // Masquer l'image sectionn�e

        // Restaurer l'image transparente de fond
        mGameObjectTransparent.SetActive(true);
    }
    void HighlightPieceAndLocation()
    {
        ClearHighlight();

        //GameObject piece = GetRandomPiece();
        List<GameObject> pieces = GetRandomPiece();
        while (pieces[0].transform.position == pieces[1].transform.position)
        {
            pieces = GetRandomPiece();
        }
        foreach (GameObject piece in pieces) {
            piece.GetComponent<TileMovement>().HighlightPiece(true);
            piece.SetActive(true);
        }


        //mTileGameObjects[index].GetComponent<TileMovement>().HighlightPiece(true);



        //if (piece != null)
        //{
        //    piece.GetComponent<TileMovement>().HighlightPiece(true);
        //}
    }

    void ClearHighlight()
    {
        foreach (var piece in mTileGameObjects)
        {
            if (piece != null)
            {
                piece.GetComponent<TileMovement>().HighlightPiece(false);
            }
        }

        foreach (var piece in mTileGOExample)
        {
            if (piece != null)
            {
                piece.GetComponent<TileMovement>().HighlightPiece(false);
            }
        }
    }

    //GameObject GetRandomPiece()
    List<GameObject> GetRandomPiece()
    {
        List<GameObject> pieces = new List<GameObject>();

        foreach (var piece in mTileGameObjects)
        {
            //if (piece != null && piece.GetComponent<TileMovement>() != null)
            if (piece != null)
            {
                pieces.Add(piece);
            }
        }

        foreach (var piece in mTileGOExample) {
            if (piece != null)
            {
                pieces.Add(piece);
            }
        }

        if (pieces.Count > 0)
        {
            int index = UnityEngine.Random.Range(0, pieces.Count/2);
            List<GameObject> gameObjects = new List<GameObject>();
            gameObjects.Add(pieces[index]);
            gameObjects.Add(pieces[index + 12]);
            return gameObjects;
        }

        return null;
    }

    Sprite LoadBaseTexture()
    {
        Texture2D tex = SpriteUtils.LoadTexture(imageFilename);
        if (!tex.isReadable)
        {
            Debug.Log("Error: Texture is not readable");
            return null;
        }

        if (tex.width % Tile.tileSize != 0 || tex.height % Tile.tileSize != 0)
        {
            Debug.Log("Error: Image must be of size that is multiple of <" + Tile.tileSize + ">");
            return null;
        }

        Texture2D newTex = new Texture2D(
            tex.width + Tile.padding * 2,
            tex.height + Tile.padding * 2,
            TextureFormat.ARGB32,
            false);

        for (int x = 0; x < newTex.width; ++x)
        {
            for (int y = 0; y < newTex.height; ++y)
            {
                newTex.SetPixel(x, y, Color.white);
            }
        }

        for (int x = 0; x < tex.width; ++x)
        {
            for (int y = 0; y < tex.height; ++y)
            {
                Color color = tex.GetPixel(x, y);
                color.a = 1.0f;
                newTex.SetPixel(x + Tile.padding, y + Tile.padding, color);
            }
        }
        newTex.Apply();

        Sprite sprite = SpriteUtils.CreateSpriteFromTexture2D(
            newTex,
            0,
            0,
            newTex.width,
            newTex.height);
        return sprite;
    }

    Sprite CreateTransparentView(Texture2D tex)
    {
        Texture2D newTex = new Texture2D(
            tex.width,
            tex.height,
            TextureFormat.ARGB32,
            false);

        for (int x = 0; x < newTex.width; ++x)
        {
            for (int y = 0; y < newTex.height; ++y)
            {
                Color c = tex.GetPixel(x, y);
                if (x > Tile.padding &&
                   x < (newTex.width - Tile.padding) &&
                   y > Tile.padding &&
                   y < (newTex.height - Tile.padding))
                {
                    c.a = ghostTransparency;
                }
                newTex.SetPixel(x, y, c);
            }
        }
        newTex.Apply();

        Sprite sprite = SpriteUtils.CreateSpriteFromTexture2D(
            newTex,
            0,
            0,
            newTex.width,
            newTex.height);
        return sprite;
    }

    void SetCameraPosition()
    {
        Camera.main.transform.position = new Vector3(mBaseSpriteOpaque.texture.width / 2,
            mBaseSpriteOpaque.texture.height / 2, -10.0f);
        int smaller_value = Mathf.Min(mBaseSpriteOpaque.texture.width, mBaseSpriteOpaque.texture.height);
        Camera.main.orthographicSize = smaller_value * 0.8f;
    }

    public static GameObject CreateGameObjectFromTile(Tile tile, Boolean example=false)
    {
        GameObject obj = new GameObject();

        obj.name = "TileGameObe_" + tile.xIndex.ToString() + "_" + tile.yIndex.ToString();
        if (example) {
            obj.name += "_example";
        }

        obj.transform.position = new Vector3(tile.xIndex * Tile.tileSize, tile.yIndex * Tile.tileSize, 0.0f);

        SpriteRenderer spriteRenderer = obj.AddComponent<SpriteRenderer>();
        spriteRenderer.sprite = SpriteUtils.CreateSpriteFromTexture2D(
            tile.finalCut,
            0,
            0,
            Tile.padding * 2 + Tile.tileSize,
            Tile.padding * 2 + Tile.tileSize);

        BoxCollider2D box = obj.AddComponent<BoxCollider2D>();

        TileMovement tileMovement = obj.AddComponent<TileMovement>();
        tileMovement.tile = tile;

        return obj;
    }

    IEnumerator Coroutine_CreateJigsawTiles()
    {
        Texture2D baseTexture = mBaseSpriteOpaque.texture;
        numTileX = baseTexture.width / Tile.tileSize;
        numTileY = baseTexture.height / Tile.tileSize;

        Debug.Log("numTileX: " + numTileX + " numTileY: " + numTileY);

        mTiles = new Tile[numTileX, numTileY];
        mTileGameObjects = new GameObject[numTileX, numTileY];
        mTileGOExample = new GameObject[numTileX, numTileY];
        endPositions = new Vector3[numTileX, numTileY];

        for (int i = 0; i < numTileX; i++)
        {
            for (int j = 0; j < numTileY; j++)
            {
                mTiles[i, j] = CreateTile(i, j, baseTexture);

                mTileGameObjects[i, j] = CreateGameObjectFromTile(mTiles[i, j]);
                mTileGOExample[i, j] = CreateGameObjectFromTile(mTiles[i, j], true);
                endPositions[i, j] = mTileGameObjects[i, j].transform.position;

                if (parentForTiles != null)
                {
                    mTileGameObjects[i, j].transform.SetParent(parentForTiles);
                    mTileGOExample[i, j].transform.SetParent(parentForTiles);
                }

                yield return null;
            }
        }

        menu.SetEnableBottomPanel(true);
        menu.btnPlayOnClick = startGame;

        HideSectionedImage();
    }

    Tile CreateTile(int i, int j, Texture2D baseTexture)
    {
        Tile tile = new Tile(baseTexture);
        tile.xIndex = i;
        tile.yIndex = j;

        if (i == 0)
        {
            tile.SetCurveType(Tile.Direction.LEFT, Tile.PosNegType.NONE);
        }
        else
        {
            Tile leftTile = mTiles[i - 1, j];
            Tile.PosNegType rightOp = leftTile.GetCurveType(Tile.Direction.RIGHT);
            tile.SetCurveType(Tile.Direction.LEFT, rightOp == Tile.PosNegType.NEG ?
                Tile.PosNegType.POS : Tile.PosNegType.NEG);
        }

        if (j == 0)
        {
            tile.SetCurveType(Tile.Direction.DOWN, Tile.PosNegType.NONE);
        }
        else
        {
            Tile downTile = mTiles[i, j - 1];
            Tile.PosNegType upOp = downTile.GetCurveType(Tile.Direction.UP);
            tile.SetCurveType(Tile.Direction.DOWN, upOp == Tile.PosNegType.NEG ?
                Tile.PosNegType.POS : Tile.PosNegType.NEG);
        }

        if (i == numTileX - 1)
        {
            tile.SetCurveType(Tile.Direction.RIGHT, Tile.PosNegType.NONE);
        }
        else
        {
            float toss = UnityEngine.Random.Range(0f, 1f);
            if (toss < 0.5f)
            {
                tile.SetCurveType(Tile.Direction.RIGHT, Tile.PosNegType.POS);
            }
            else
            {
                tile.SetCurveType(Tile.Direction.RIGHT, Tile.PosNegType.NEG);
            }
        }

        if (j == numTileY - 1)
        {
            tile.SetCurveType(Tile.Direction.UP, Tile.PosNegType.NONE);
        }
        else
        {
            float toss = UnityEngine.Random.Range(0f, 1f);
            if (toss < 0.5f)
            {
                tile.SetCurveType(Tile.Direction.UP, Tile.PosNegType.POS);
            }
            else
            {
                tile.SetCurveType(Tile.Direction.UP, Tile.PosNegType.NEG);
            }
        }

        tile.Apply();
        return tile;
    }

    private IEnumerator Coroutine_MoveOverSeconds(GameObject objectToMove, Vector3 end, float seconds)
    {
        float elapsedTime = 0.0f;
        Vector3 startingPosition = objectToMove.transform.position;
        while (elapsedTime < seconds)
        {
            objectToMove.transform.position = Vector3.Lerp(
                startingPosition, end, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }
        objectToMove.transform.position = end;
    }

    void Shuffle(GameObject obj, int counter, int i, int j)
    {
       float minimumDistance = Mathf.Max(obj.GetComponent<SpriteRenderer>().bounds.size.x, obj.GetComponent<SpriteRenderer>().bounds.size.y);
        
        if (regions.Count == 0)
        {
            regions.Add(new Rect(-10 - minimumDistance, -100.0f, 1.0f, (numTileY) * obj.GetComponent<SpriteRenderer>().bounds.size.y));
            regions.Add(new Rect((numTileX-1) * minimumDistance + 10, -100.0f, 1.0f, (numTileY) * obj.GetComponent<SpriteRenderer>().bounds.size.y));
            regions.Add(new Rect(-10 - 2 * minimumDistance, -100.0f, 1.0f, (numTileY) * obj.GetComponent<SpriteRenderer>().bounds.size.y));
            regions.Add(new Rect((numTileX) * minimumDistance + 10, -100.0f, 1.0f, (numTileY) * obj.GetComponent<SpriteRenderer>().bounds.size.y));
        }

        bool positionFound = false;
        Vector3 pos = Vector3.zero;

        int regionIndex = UnityEngine.Random.Range(0, regions.Count);
        regionIndex = counter % regions.Count;

        float x = regions[regionIndex].xMin;
        float y = (((int)(counter / regions.Count) * minimumDistance)) + regions[regionIndex].yMin;

        pos = new Vector3(x, y, 0.0f);

        endPositions[i, j] = pos;

        Coroutine moveCoroutine = StartCoroutine(Coroutine_MoveOverSeconds(obj, pos, 1.0f));
        activeCoroutines.Add(moveCoroutine);
    }

    IEnumerator Coroutine_Shuffle()
    {
        int counter = 0;
        for (int i = 0; i < numTileX; ++i)
        {
            for (int j = 0; j < numTileY; ++j)
            {
                Shuffle(mTileGameObjects[i, j], counter, i, j);
                counter++;
                yield return null;
            }
        }

        foreach (var item in activeCoroutines)
        {
            if (item != null)
            {
                yield return null;
            }
        }

        OnFinishedShuffling();
    }

    public void startGame() 
    {
        ShuffleTiles();
        StartFlowTimer();
    }
    public void ShuffleTiles()
    {
        StartCoroutine(Coroutine_Shuffle());
    }

    void OnFinishedShuffling()
    {
        activeCoroutines.Clear();

        menu.SetEnableBottomPanel(false);
        StartCoroutine(Coroutine_CallAfterDelay(() => menu.SetEnableTopPanel(true), 1.0f));
        GameApp.Instance.TileMovementEnabled = true;

        StartTimer();

        for (int i = 0; i < numTileX; ++i)
        {
            for (int j = 0; j < numTileY; ++j)
            {
                TileMovement tm = mTileGameObjects[i, j].GetComponent<TileMovement>();
                TileMovement.OnTilePlacedCorrectly += OnTileInPlace;
                TileMovement.OnTilePlacedIncorrectly += vibrationEventManager.HandlePlacementError;
                SpriteRenderer spriteRenderer = tm.gameObject.GetComponent<SpriteRenderer>();
                Tile.tilesSorting.BringToTop(spriteRenderer);

                SpriteRenderer sr = mTileGOExample[i, j].GetComponent<SpriteRenderer>();
                Tile.tilesSorting.BringToTop(sr);
            }
        }

        menu.SetTotalTiles(numTileX * numTileY);
    }

    IEnumerator Coroutine_CallAfterDelay(System.Action function, float delay)
    {
        yield return new WaitForSeconds(delay);
        function();
    }

    public void StartTimer()
    {
        StartCoroutine(Coroutine_Timer());
    }

    IEnumerator Coroutine_Timer()
    {
        while (true)
        {
            yield return new WaitForSeconds(1.0f);
            GameApp.Instance.SecondsSinceStart += 1;

            menu.SetTimeInSeconds(GameApp.Instance.SecondsSinceStart);
        }
    }

    public void StopTimer()
    {
        StopCoroutine(Coroutine_Timer());
    }

    //public void ShowOpaqueImage()
    //{
       // mGameObjectOpaque.SetActive(true);
   // }

    //public void HideOpaqueImage()
    //{
       // mGameObjectOpaque.SetActive(false);
   // }

    void OnTileInPlace(TileMovement tm)
    {
        Debug.Log("OnTileInPlace called for tile: " + tm.name);

        if (!tm.enabled)
        {
            Debug.LogWarning("OnTileInPlace called for a tile that is already disabled: " + tm.name);
            return;
        }

        GameApp.Instance.TotalTilesInCorrectPosition += 1;
        Debug.Log("TotalTilesInCorrectPosition: " + GameApp.Instance.TotalTilesInCorrectPosition);

        tm.enabled = false;
        Destroy(tm);

        SpriteRenderer spriteRenderer = tm.gameObject.GetComponent<SpriteRenderer>();
        Tile.tilesSorting.Remove(spriteRenderer);

        if (GameApp.Instance.TotalTilesInCorrectPosition == numTileX * numTileY)
        {
            Debug.Log("All tiles are in place. Game complete.");
            menu.SetEnableTopPanel(false);
            menu.SetEnableGameCompletionPanel(true);

            GameApp.Instance.SecondsSinceStart = 0;
            GameApp.Instance.TotalTilesInCorrectPosition = 0;
        }
        else
        {
            menu.SetTilesInPlace(GameApp.Instance.TotalTilesInCorrectPosition);
        }

        AdjustGameDifficulty(false);
        RestartTimer();
    }
}